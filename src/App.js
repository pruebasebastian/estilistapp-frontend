import styles from './styles/App.module.css'

export const App = () => {
  return (
    <div className={styles.container}>
      <h1 className={styles.title}>Estilistapp</h1>
    </div>
  )
}

export default App
